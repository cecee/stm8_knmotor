
/* Includes ------------------------------------------------------------------*/
#include "stm8s.h"
#include "stm8s_uart1.h"
#include "stm8s_spi.h"
#include "stm8s_exti.h"
#include "stm8s_gpio.h"
#include "stm8s_flash.h"
#include "stm8s_tim1.h"
#include "stm8s_tim4.h"
#include "stm8s_beep.h"
#include "stm8s_i2c.h"
#include "./kn_motor/kn_motor.h"
#include "./kn_pwm/kn_pwm.h"
#include "./util/util.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>



//void Init_ADC1(void);
//void PWM_4ch_init(void);
//void process_Rs232(void);


//void Power_ON(void);
//void Power_OFF(void);
//void check_update_key(void);
//void Wait_Delay(u16 data);
void Beep_On(void);
void CLK_Configuration(void);
void GPIO_Configuration(void);
//void check_botton(void);
//void check_botton_test(void);
//void prn_read_flash(u16 add);
//void lisence_out(void);

//////////////////////

void Timer1_Init(void);
void Timer4_Init(void);
void Timer2_Init(void);
u8 i;

void delay_delay(unsigned int mul);

u16 timer4_cnt=0;
u32 timer_1ms=0;

//u16 Wait_tmp=0;
u16 Beep_Time=0;
u16  sec_cnt=0;
u8 Beep_flag=0;
u8 SET_BEEP=1;

int fputc(int ch, FILE *f)
{
	u8 temp[1]={ch};
	//HAL_UART_Transmit(&huart4, temp, 1, 2);
        CUartTxChar(temp[0]); //udelay(1);
	return(ch);
}

void BEEP_ON()
{
  Beep_Time=0; 
  Beep_flag=1;///
  BEEP_Cmd(ENABLE);	///
}

/*
void Get_eeprom(void)
{
	u8 x;
	FLASH_Unlock(FLASH_MEMTYPE_DATA);

	UnionBdip.blisn[2]=FLASH_ReadByte(0x4000+2);
	UnionBdip.blisn[3]=FLASH_ReadByte(0x4000+3);//비교하는 대상
        UnionOnOFF.blisn[2]=FLASH_ReadByte(0x4004+2);
        UnionOnOFF.blisn[3]=FLASH_ReadByte(0x4004+3);
      
	FLASH_Lock(FLASH_MEMTYPE_DATA);
        
  	if((UnionBdip.blisn[3]+UnionBdip.blisn[2]) != 0xff) 
  	{
          x=(GPIO_ReadInputData(GPIOB))&0x0f;
          x=(GPIO_ReadInputData(GPIOB))&0x0f;
          UnionBdip.blisn[2]=~x;
          UnionBdip.blisn[3]= x;
          FLASH_Unlock(FLASH_MEMTYPE_DATA);
          FLASH_ProgramWord(0x4000,UnionBdip.wlisn);
          FLASH_Lock(FLASH_MEMTYPE_DATA);            

        }
        
        if(UnionOnOFF.blisn[2] != 0xAA) 
  	{

          UnionOnOFF.blisn[2]=0xAA;
          UnionOnOFF.blisn[3]= 0x00;//DC IN POWER OFF
          FLASH_Unlock(FLASH_MEMTYPE_DATA);
          FLASH_ProgramWord(0x4004,UnionOnOFF.wlisn);
          FLASH_Lock(FLASH_MEMTYPE_DATA);  

        }
        
}
*/

  
void main(void)
{
  CLK_Configuration(); /* Configures clocks */
  GPIO_Configuration();/* Configures GPIOs */
  UART1_DeInit(); // Initialize serial
  UART1_Init(115200, UART1_WORDLENGTH_8D, UART1_STOPBITS_1, UART1_PARITY_NO, UART1_SYNCMODE_CLOCK_DISABLE, UART1_MODE_TXRX_ENABLE);
  UART1_ITConfig(UART1_IT_RXNE,ENABLE); //test
  Timer4_Init();
  Timer2_Init();
  //BEEP_DeInit();
  //BEEP_Init(BEEP_FREQUENCY_1KHZ);//1KHZ
  //BEEP_Cmd(DISABLE);
  PWM_init();
  enableInterrupts();
  
 ////set beef init 
  gValue.beep_ms=0;
  gValue.beep_period_ms=1000;
  gValue.beep_on_ms=100;
  gValue.beep_status=0;
////
  printf("Start!!\r\n");
  car_init();
  while (1)
  {
    /*
	if(P_STATUS.CurONOFF)
		{
	//		process_Rs232();
      if(COIN_CNT>0){
        coin_temp=COIN_CNT;
        COIN_CNT=0;
        for(i=0;i<coin_temp;i++){
          GPIO_WriteLow(GPIOC, EX_COIN_PC2);
          Udelay(2000);
          GPIO_WriteHigh(GPIOC, EX_COIN_PC2);
          Udelay(20000);
        }        
        coin_temp=0;
      }
*/      
   }

}


void FlashInit(void)
{
	FLASH_Unlock(FLASH_MEMTYPE_DATA);
	FLASH_ProgramWord(0x4000,0xffffffff);
	FLASH_ProgramWord(0x4004,0xffffffff);
	FLASH_Lock(FLASH_MEMTYPE_DATA);
}


//===================================================
//             TIMER2 10millisec Timer
//===================================================
void Timer2_Init() {
	
  TIM2->SR1 = 0;       // clear overflow flag
  TIM2->PSCR = 7;     // Prescaler to divide Fcpu by 128: 8 us clock.// Max CPU freq = 16 MHz
  TIM2->ARRH = 0x04; // per25msec, 16bit
  TIM2->ARRL = 0xE2;
  TIM2->IER |= 0x1; // Update interrupt enabled
  TIM2->CR1 |= 0x1; // Counter enabled
  
}


#pragma vector = 0x0F
__interrupt void Timer2_ISR(void)
{
  TIM2->SR1 = 0;
  //GPIO_WriteReverse(GPIOC, pDIS);
  sub_10ms();  
}

//===================================================
//             TIMER4 1mSec Timer
//===================================================

void Timer4_Init(void)
{
	//1ms setting
        TIM4->SR1 = 0;       // clear overflow flag
	//TIM4->PSCR = 6;     // Prescaler to divide Fcpu by 64: 4 us clock.// Max CPU freq = 16 MHz
        TIM4->PSCR = 7;     // Prescaler to divide Fcpu by 128: 4 us clock.// Max CPU freq = 16 MHz
	TIM4->ARR = 125;    // 125*4*2 = 1000 us.
	TIM4->IER = 0x01;   // Enable interrupt
	TIM4->CR1 = 0x01;   // Start timer
}
#pragma vector = 25
__interrupt void Timer4_ISR(void)
{

  TIM4->SR1 = 0;      // clear overflow flag
  //timer_1ms++;
  sub_1ms();
  
#if 0 
  if(timer4_cnt>10){
    timer4_cnt=0;    
    gValue.msec=timer_1ms;
    if(gValue.dir==1){ gValue.beep_ms++;}  
    else gValue.beep_ms=gValue.beep_period_ms-100;
    
    if(gValue.beep_period_ms < gValue.beep_ms) gValue.beep_ms=0;
    //if(gValue.beep_ms<gValue.beep_on_ms && gValue.stop==0) 
    if(gValue.beep_ms<gValue.beep_on_ms)       
    {
      if(gValue.beep_status==0){
        gValue.beep_status=1;
        //_printf("BEEP ON\r\n",0);
        if(gValue.stop==0) 
          BEEP_Cmd(ENABLE);
      }
    }
    else {
      if(gValue.beep_status){
        gValue.beep_status=0;
        //_printf("BEEP OFF\r\n",0);
        BEEP_Cmd(DISABLE);
      }
    }
    
  }
#endif
  //Wait_tmp++;
  //Beep_Time++;

}

#pragma vector=0x14
__interrupt void UART1_RX_IRQHandler(void)
{
  //u8 data;
  //data = UART1_ReceiveData8();

}
#if 0

#pragma vector=6 //5A 6B 7D 
__interrupt void EXTI_PORTB_IRQHandler(void)
{
   // disableInterrupts();
   // Udelay(5000);//20msec
   PWR_IRQ=1;

}


 
#pragma vector=8 //5A 6B 7D 
__interrupt void EXTI_PORTD_IRQHandler(void)
{
	//if(GPIO_ReadInputPin(GPIOD,EMER_IN)==0) EMER_CNT++;
	//else if(GPIO_ReadInputPin(GPIOD,COIN_IN)==0) {
	//	COIN_CNT++;
	//}
}

#endif
void CLK_Configuration(void)
{
  /* Fmaster = 16MHz */
  CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
  //CLK-> CKDIVR | = 0x00;
}

void GPIO_Configuration(void)
{

  GPIO_DeInit(GPIOA); /* IR-Port */
   
  GPIO_DeInit(GPIOB);/* GPIOB reset */
  //GPIO_Init(GPIOB, GPIO_PIN_0, GPIO_MODE_IN_PU_NO_IT);  
  //GPIO_Init(GPIOB, GPIO_PIN_1, GPIO_MODE_IN_PU_NO_IT); 
  //GPIO_Init(GPIOB, GPIO_PIN_2, GPIO_MODE_IN_PU_NO_IT); 
  //GPIO_Init(GPIOB, GPIO_PIN_3, GPIO_MODE_IN_PU_NO_IT); 
  
  GPIO_Init(GPIOB, DIR_FF, GPIO_MODE_IN_PU_NO_IT);   
  GPIO_Init(GPIOB, DIR_RR, GPIO_MODE_IN_PU_NO_IT);

//LED_CTL MRES_PIN
  GPIO_DeInit(GPIOC); // C-Port 
  GPIO_Init(GPIOC, pDIR_FF, GPIO_MODE_OUT_PP_HIGH_FAST);
  GPIO_Init(GPIOC, pDIR_RR, GPIO_MODE_OUT_PP_HIGH_FAST);
  GPIO_Init(GPIOC, pDIS, GPIO_MODE_OUT_PP_HIGH_FAST);
  GPIO_Init(GPIOC, pBK, GPIO_MODE_OUT_PP_HIGH_FAST); 
  GPIO_Init(GPIOC, pRTN, GPIO_MODE_IN_PU_NO_IT);
  
 //----------------------------------------------------------------------
  GPIO_DeInit(GPIOD); 
  GPIO_Init(GPIOD, pLED_FF, GPIO_MODE_OUT_PP_HIGH_FAST);
  GPIO_Init(GPIOD, pLED_RR, GPIO_MODE_OUT_PP_HIGH_FAST);
  GPIO_Init(GPIOD, pTEST, GPIO_MODE_OUT_PP_HIGH_FAST);
  
  //---------------------------------------------------------------------
 
  GPIO_DeInit(GPIOE); 
  GPIO_Init(GPIOE, pRELAY, GPIO_MODE_OUT_PP_HIGH_FAST); 
  

  GPIO_DeInit(GPIOF); 
  //GPIO_Init(GPIOF, MUTE_RELAY, GPIO_MODE_OUT_PP_HIGH_SLOW);//MUTE
#if 0
  EXTI_DeInit();    
 // EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOB, EXTI_SENSITIVITY_FALL_LOW);
  EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOD, EXTI_SENSITIVITY_FALL_ONLY);
  EXTI_SetTLISensitivity(EXTI_TLISENSITIVITY_FALL_ONLY);  
 // EXTI_SetTLISensitivity(EXTI_TLISENSITIVITY_FALL_ONLY);  
#endif
  
}






void delay_delay(unsigned int mul)
{
   for(; mul>0; mul--);
}









